package ru.web.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import ru.web.models.Post;
import ru.web.models.User;
import ru.web.services.PostsService;
import ru.web.services.UsersService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;


@Controller
public class PostsController {

    @Autowired
    private PostsService postsService;

    @Autowired
    private UsersService usersService;

    @GetMapping("/posts")
    public String getPostsPage(Model model){
        model.addAttribute("posts",postsService.findAllPosts());
        return "posts_page";
    }

    @PostMapping("/posts")
    public String addPost(@RequestParam("text") String text,
                          @RequestParam("author") String author,
                          @RequestParam("picture") MultipartFile file) throws IOException {

        Files.copy(file.getInputStream(), Paths.get("C://files/" + file.getOriginalFilename()));

        if(usersService.findUser(author).isPresent()){
            User user = usersService.findUser(author).get();
            postsService.addPost(new Post(text, user));
        } else {
            System.out.println("No such user");
        }
        return "redirect:/posts";
    }

}
