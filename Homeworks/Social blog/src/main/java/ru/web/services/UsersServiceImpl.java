package ru.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.web.models.User;
import ru.web.repositories.UsersRepository;

import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Optional<User> findUser(String nickname) {
        Optional<User> user = Optional.of(usersRepository.findUserByNickname(nickname));
        return user;
    }

}
