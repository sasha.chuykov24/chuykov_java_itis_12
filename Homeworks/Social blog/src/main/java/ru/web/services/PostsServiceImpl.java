package ru.web.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.web.models.Post;
import ru.web.repositories.PostsRepository;

import java.util.List;

@Service
public class PostsServiceImpl implements PostsService {

    @Autowired
    private PostsRepository postsRepository;

    @Override
    public void addPost(Post post) {
        postsRepository.save(post);
    }

    @Override
    public List<Post> findAllPosts(){
        return postsRepository.findAll();
    }
}
