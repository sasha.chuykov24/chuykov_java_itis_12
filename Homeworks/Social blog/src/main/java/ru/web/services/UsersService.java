package ru.web.services;

import ru.web.models.User;

import java.util.Optional;

public interface UsersService {
    Optional<User> findUser(String nickname);
}
