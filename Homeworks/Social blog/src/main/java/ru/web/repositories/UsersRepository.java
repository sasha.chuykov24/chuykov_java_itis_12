package ru.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.web.models.User;

public interface UsersRepository extends JpaRepository<User, Long> {
    User findUserByNickname(String string);
}
