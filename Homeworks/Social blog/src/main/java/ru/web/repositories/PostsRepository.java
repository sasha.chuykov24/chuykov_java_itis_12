package ru.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.web.models.Post;


public interface PostsRepository extends JpaRepository<Post, Long> {

}
