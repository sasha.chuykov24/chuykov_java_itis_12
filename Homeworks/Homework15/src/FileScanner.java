import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileScanner {
    InputStream input;
    int code;

    public FileScanner(String fileName){
        try {
            this.input = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            throw new IllegalArgumentException("Такого файла нет");
        }
    }

    public int nextInt() {
        StringBuilder lineCode = new StringBuilder();
        int i = 1;
        int num = 0;
        try {
            code = input.read();
            if(code < 48 || code > 57){
                throw new IllegalArgumentException("Некорректный ввод");
            }
            while(code >= 48 && code <= 57){
                lineCode.append((char) code);
                code = input.read();
            }
            for(int j = lineCode.length()-1; j >= 0; j--){
                num = num + ((int) lineCode.charAt(j) - 48) * i;
                i = i * 10;
            }
            return num;
        } catch (IOException e) {
            throw new IllegalArgumentException("Проблема с чтением файла");
        }
    }

    public String nextLine() {
        StringBuilder builder = new StringBuilder();
        try {
            while((char)(code) != '\n'){
                builder.append((char)code);
                code = input.read();
            }
            code = input.read();
            return builder.toString();
        } catch (IOException e) {
            throw new IllegalArgumentException("Проблема с чтением файла");
        }
    }
}
