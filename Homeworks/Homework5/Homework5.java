import java.util.Scanner;

class Homework5{
	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		int mas[] = new int[6];
		for (int i = 0; i < mas.length; i++) {
			mas[i] = scanner.nextInt();
		}
		for(int i=0; i<mas.length/2; i++){
			int num = mas[i];
			mas[i] = mas[mas.length - i - 1];
			mas[mas.length - i - 1] = num;
		}
		for (int i = 0; i < mas.length; i++) {
			System.out.print(mas[i]);
		}
	}
}