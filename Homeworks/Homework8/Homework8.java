import java.util.Scanner;

class Homework8{

	public static int fib(int n){
		int result = 0;
		if(n == 0){
			return 0;
		}
		if(n == 1){
			return 1;
		}
		result = fib(n-1) + fib(n-2); 
		return result;
	}

	public static void main(String[] args){
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		System.out.println(fib(n));
	}
}