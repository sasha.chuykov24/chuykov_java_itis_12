package ru.itis12;

import ru.itis12.models.Chat;
import ru.itis12.models.User;
import ru.itis12.repositories.UsersRepository;
import ru.itis12.repositories.UsersRepositoryJdbcImpl;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Scanner;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/chat";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty2474";


    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            ServerSocketExample serverSocketExample = new ServerSocketExample(connection);
            serverSocketExample.start(7777);
            /*UsersRepositoryJdbcImpl s = new UsersRepositoryJdbcImpl(connection);
            System.out.println(s.findByNickname("Александр"));*/

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}

