package ru.itis12.repositories;

import ru.itis12.models.User;

public interface UsersRepository extends CrudRepository<User> {
    Boolean findByNickname(String nickName);
}
