package ru.itis12.repositories;

import ru.itis12.models.Chat;
import ru.itis12.models.User;

public interface ChatRepository extends CrudRepository<Chat> {
    Boolean findByName(String Name);
}
