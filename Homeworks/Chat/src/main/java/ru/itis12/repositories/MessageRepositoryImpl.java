package ru.itis12.repositories;

import ru.itis12.models.Message;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MessageRepositoryImpl implements MessageRepository {

    //language=SQl
    private static final String SQL_INSERT_MESSAGE = "insert into message (text, user_id, chat_id, time) values (?,?,?,?)";

    //language=SQl
    private static final String SQL_SELECT_30_MESSAGES_BY_CHAT_ID = "select m.text as text,\n" +
            "       m.user_id as user_id,\n" +
            "       m.chat_id as chat_id,\n" +
            "       m.time as time,\n" +
            "       cu.nickname as nickname\n" +
            "from message m\n" +
            "left join chat_user cu on m.user_id = cu.id\n" +
            "where chat_id = ?\n" +
            "order by time desc\n" +
            "limit 30;";

    private Connection connection;

    public MessageRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void save(Message object) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MESSAGE);
            preparedStatement.setString(1, object.getText());
            preparedStatement.setInt(2, object.getUserId());
            preparedStatement.setInt(3, object.getChatId());
            preparedStatement.setTimestamp(4,object.getTime());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Message find(Integer id) {
        return null;
    }

    @Override
    public List<Message> findAll() {

        return null;
    }

    @Override
    public List<Message> findLast30(Integer id) {
        ArrayList<Message> messages = new ArrayList<>();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_30_MESSAGES_BY_CHAT_ID);
            preparedStatement.setInt(1,id);
            ResultSet row = preparedStatement.executeQuery();
            while(row.next()){
                messages.add(new Message(row.getString("nickname") + ": " +
                        row.getString("text")));
            }
            return messages;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
