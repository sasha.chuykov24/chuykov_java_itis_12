package ru.itis12.repositories;

import ru.itis12.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UsersRepositoryJdbcImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from chat_user";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from chat_user where id = ";

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select id as id, nickname as nickname from chat_user where nickname = ";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into chat_user(nickname) values (?)";

    private Connection connection;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException{
            return new User(
                    row.getInt("id"),
                    row.getString("nickname")
            );
        }
    };
    public UsersRepositoryJdbcImpl(Connection connection) {
        this.connection = connection;
    }

    public User findByFirstName(String firstName) {
        return null;
    }

    public void save(User object) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_USER);
            statement.setString(1, object.getNickname());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException();
        }
    }

    public void update(User object) {

    }

    public void delete(Integer id) {

    }

    public User find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            statement.close();
            return userRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Boolean findByNickname(String nickName) {
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_NAME + "'" + nickName + "'");
            if(resultSet.next()){
                return resultSet.getString("nickname").equals(nickName);
            }
            return false;
            //statement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<User> findAll() {
        try {
            List<User> result = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while(resultSet.next()){
                User user = userRowMapper.mapRow(resultSet);
                result.add(user);
            }
            statement.close();
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
