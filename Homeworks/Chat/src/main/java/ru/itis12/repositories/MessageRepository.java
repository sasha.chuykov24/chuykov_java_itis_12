package ru.itis12.repositories;

import ru.itis12.models.Message;

import java.util.List;

public interface MessageRepository extends CrudRepository<Message> {
    List<Message> findLast30(Integer id);
}
