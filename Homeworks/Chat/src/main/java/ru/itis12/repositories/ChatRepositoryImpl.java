package ru.itis12.repositories;

import ru.itis12.models.Chat;
import ru.itis12.models.User;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ChatRepositoryImpl implements ChatRepository {
    private Connection connection;

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select * from chat where name = ";

    //language=SQL
    private static final String SQL_INSERT_CHAT = "insert into chat(name) values (?)";

    public ChatRepositoryImpl(Connection connection){
        this.connection = connection;
    }

    /*private RowMapper<Chat> chatRowMapper = new RowMapper<Chat>() {
        public Chat mapRow(ResultSet row) throws SQLException{
            return new Chat(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("startDate")
            );
        }
    };*/

    @Override
    public void save(Chat object) {
        try {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_CHAT);
            statement.setString(1, object.getName());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Chat object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Chat find(Integer id) {

        return null;
    }

    public Boolean findByName(String name) {
        try{
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_NAME + "'" + name + "'");
            if(row.next()){
                return row.getString("name").equals(name);
            }
            return false;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Chat> findAll() {
        return null;
    }
}
