package ru.itis12;

import ru.itis12.models.Chat;
import ru.itis12.models.Message;
import ru.itis12.models.User;
import ru.itis12.repositories.*;

import java.io.*;
import java.net.Socket;
import java.net.ServerSocket;
import java.sql.Connection;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ServerSocketExample {
    private Connection connection;
    private List<SocketClient> clientsWithoutRooms;
    private Map<String, List<SocketClient>> clientsInRooms;

    public ServerSocketExample(Connection connection) {
        this.clientsWithoutRooms = new ArrayList<>();
        this.clientsInRooms = new HashMap<>();
        this.connection = connection;
    }

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                Socket client = serverSocket.accept();
                SocketClient socketClient = new SocketClient(client);
                this.clientsWithoutRooms.add(socketClient);
                socketClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public class SocketClient extends Thread {
        private Socket client;
        private BufferedReader from;
        private PrintWriter to;
        private String currentRoom;
        private Integer roomId;
        private Integer userId;

        private UsersRepository usersRepository;
        private ChatRepository chatRepository;
        private MessageRepository messageRepository;
        private ChatUserChatRepository chatUserChatRepository;



        public SocketClient(Socket socket) {
            this.client = socket;
            try {
                InputStream clientInputStream = client.getInputStream();
                this.from = new BufferedReader(new InputStreamReader(clientInputStream));
                this.to = new PrintWriter(client.getOutputStream(), true);

                usersRepository = new UsersRepositoryJdbcImpl(connection);
                chatRepository = new ChatRepositoryImpl(connection);
                messageRepository = new MessageRepositoryImpl(connection);
                chatUserChatRepository = new ChatUserChatRepositoryImpl(connection);
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            try {
                to.println("Введите nickname: ");
                String nickname = from.readLine();

                while (nickname == null) {
                    nickname = this.from.readLine();
                }

                // попадаем сюда, и здесь nickname уже не null
                // TODO: проверить в базе данных, есть ли такой пользователь
                if(!usersRepository.findByNickname(nickname)){
                    usersRepository.save(new User(nickname));

                }

                to.println("Введите комнату: ");
                String roomName = from.readLine();

                while (roomName == null) {
                    roomName = from.readLine();
                }

                // поподаем сюда, и здесь roomName уже не null
                // TODO: проверить в базе данных, есть ли такая комната
                if (!chatRepository.findByName(roomName)) {
                    chatRepository.save(new Chat(roomName));
                }
                // TODO: проверить, есть ли эта комната в clientsInRooms
                if (!clientsInRooms.containsKey(roomName)) {
                    clientsInRooms.put(roomName, new ArrayList<>());
                }

                clientsInRooms.get(roomName).add(this);
                this.currentRoom = roomName;

                //roomId = chatRepository.findByName(roomName).getId();
                //userId = usersRepository.findByNickname(nickname).getId();

                chatUserChatRepository.save(userId,roomId);

                to.println(messageRepository.findLast30(roomId));

                String message = from.readLine();
                while (message != null) {
                    messageRepository.save(new Message(message, userId, roomId, new Timestamp(System.currentTimeMillis())));
                    // TODO: отослать всем людям в этой комнате
                    for (SocketClient anotherClient : clientsInRooms.get(roomName)) {
                        anotherClient.to.println(nickname + ": " + message);
                    }
                    message = from.readLine();
                }
            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }
    }
}
