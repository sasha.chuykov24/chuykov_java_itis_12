package ru.itis12.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class User {
    private Integer id;
    private String nickname = null;

    public User(String nickname){
        this.nickname = nickname;
    }
}
