import Repository.CourseRepository;
import Repository.CourseRepositoryImpl;
import Repository.LessonRepository;
import Repository.LessonRepositoryImpl;
import models.Course;
import models.Lesson;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Main {
    private static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    private static final String DB_USER = "postgres";
    private static final String DB_PASSWORD = "qwerty2474";

    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);
            CourseRepository courseRepository = new CourseRepositoryImpl(connection);
            LessonRepository lessonRepository = new LessonRepositoryImpl(connection);

            /*Course course = courseRepository.find(1);
            System.out.println(course);*/

            /*List<Course> courseList = new ArrayList(courseRepository.findAll());
            System.out.println(courseList);*/

            Lesson lesson = lessonRepository.find(1);
            System.out.println(lesson.getCourse());



        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
