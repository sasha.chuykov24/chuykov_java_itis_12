package models;


import lombok.AllArgsConstructor;
import lombok.Data;


public class Lesson {
    private Long id;
    private String name;
    private Course course;

    @Override
    public String toString() {
        return "Lesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", course=" + course.getTitle() +
                '}';
    }

    public Lesson(Long id, String name, Course course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }
}
