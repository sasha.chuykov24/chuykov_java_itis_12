package Repository;

import models.Course;

public interface CourseRepository extends CrudRepository<Course> {
}
