package Repository;

import models.Course;
import models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class LessonRepositoryImpl implements LessonRepository {

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from lesson l left join course c on l.course_id = c.id where l.id = ";

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from lesson l join course c on l.course_id = c.id";

    Connection connection;

    public LessonRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Lesson object) {

    }

    public void update(Lesson object) {

    }

    public void delete(Integer id) {

    }

    public Lesson find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_ID + id);
            row.next();
            Course course = new Course(row.getLong(1), row.getString(2), new ArrayList());
            Lesson lesson = new Lesson(row.getLong(3),
                    row.getString(4),
                    course);
            course.getLessons().add(lesson);
            while (row.next()){
                // берём все уроки курса
                 Lesson newLesson = new Lesson(row.getLong(3),
                        row.getString(4),
                        course);
                // добавляем в этот курс новый урок
                course.getLessons().add(newLesson);
            }
            return lesson;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    //TODO
    public Collection<Lesson> findAll() {
        Map<Long, Lesson> lessons = new HashMap<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_ALL);

            while (row.next()){
                if (!lessons.containsKey((long) row.getInt(1))) {
                    // создаем его
                    Lesson newLesson = new Lesson(row.getLong(1), row.getString(2), new Course());
                    // кладем в Мапу
                    lessons.put(newLesson.getId(), newLesson);
                }
                Lesson existedLesson = lessons.get((long) row.getInt(1));
                Course course = existedLesson.getCourse();
                course.setId(row.getLong(4));
                course.setTitle(row.getString(5));
            }
            return lessons.values();
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
