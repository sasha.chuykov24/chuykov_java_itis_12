package Repository;

import java.util.Collection;
import java.util.List;

public interface CrudRepository<T> {
    void save(T object);
    void update(T object);
    void delete(Integer id);
    T find(Integer id);
    Collection<T> findAll();
}
