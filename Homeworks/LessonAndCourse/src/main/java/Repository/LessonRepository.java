package Repository;

import models.Lesson;

public interface LessonRepository extends CrudRepository<Lesson> {
}
