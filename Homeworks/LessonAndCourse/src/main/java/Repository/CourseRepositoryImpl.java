package Repository;

import models.Course;
import models.Lesson;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;


public class CourseRepositoryImpl implements CourseRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from course c left join lesson l on l.course_id = c.id";
    private static final String SQL_SELECT_BY_ID = "select * from course c left join lesson l on l.course_id = c.id where c.id = ";

    private Connection connection;

    public CourseRepositoryImpl(Connection connection) {
        this.connection = connection;
    }

    public void save(Course object) {

    }

    public void update(Course object) {

    }

    public void delete(Integer id) {

    }

    public Course find(Integer id) {
        try {
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_ID + id);
            row.next();
            Course course = new Course(row.getLong(1), row.getString(2), new ArrayList());
            do {
                // создаем урок
                Lesson lesson = new Lesson(row.getLong(3),
                        row.getString(4),
                        course);
                // добавляем в этот курс новый урок
                course.getLessons().add(lesson);
            } while (row.next());
            return course;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Collection<Course> findAll() {
        Map<Long, Course> courses = new HashMap<>();
        try {
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_ALL);

            // пробегаемся по всем строкам ResultSet
            while (row.next()) {
                // если такого курса не было
                if (!courses.containsKey((long) row.getInt(1))) {
                    // создаем его
                    Course newCourse = new Course(row.getLong(1), row.getString(2), new ArrayList());
                    // кладем в Мапу
                    courses.put(newCourse.getId(), newCourse);
                }
                // вытаскивает из мапы курс с id-шником, который указан у текушего урока
                Course existedCourse = courses.get((long) row.getInt(5));

                // создаем урок
                Lesson lesson = new Lesson(row.getLong(3),
                        row.getString(4),
                        existedCourse);
                // добавляем в этот курс новый урок
                existedCourse.getLessons().add(lesson);
            }
            return courses.values();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }

    }
}
