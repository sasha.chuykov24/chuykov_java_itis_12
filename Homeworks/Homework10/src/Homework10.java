public class Homework10 {
    public static int parseInt(char mas[]){
        int n = 0;
        int number = 0;
        int mult = 1;
        for(int i = mas.length - 1; i >= 0; i--){
            n = (int) mas[i] - 48;
            number = number + n * mult;
            mult = mult * 10;
        }
        return number;
    }
    public static void main(String[] args){
        char mas[] = {'1','2','3'};
        System.out.println(parseInt(mas));
    }
}
