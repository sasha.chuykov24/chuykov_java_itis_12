package array;

public class MainForArrayList {

    public static void main(String[] args) {
	    ArrayList list = new ArrayList();
	    for(int i = 0; i < 10; i++){
	    	list.add(i);
		}

		for(int i = 0; i < 10; i++){
			System.out.print(list.get(i) + " ");
		}
		list.add(10);
		list.add(11);
		list.add(12);

		System.out.println(" ");
		ArrayList.ArrayListIterator iterator = list.iterator();
		while(iterator.hasNext()){
			System.out.print(iterator.next() + " ");
		}

    }
}
