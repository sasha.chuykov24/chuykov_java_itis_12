package array;

public class ArrayList {
    private static final int DEFAULT_ARRAY_SIZE = 10;

    private int elements[];
    private int count;

    class ArrayListIterator {
        int current;

        public ArrayListIterator(){
            this.current = 0;
        }

        public boolean hasNext() {
            return current < count;
        }

        public int next() {
            return elements[current++];
        }
    }

    public ArrayListIterator iterator(){
        return new ArrayListIterator();
    }

    public ArrayList() {
        this.elements = new int[DEFAULT_ARRAY_SIZE];
        this.count = 0;
    }

    public void add(int element) {
        if (count < this.elements.length) {
            this.elements[count] = element;
            this.count++;
        } else {
            increase();
            add(element);
        }
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            return elements[index];
        } else {
            System.err.println("Неверный индекс");
            return -1;
        }
    }

    public void addToBegin(int element) {
        if (count < this.elements.length) {
            for (int i = count; i > 0; i--) {
                elements[i] = elements[i - 1];
            }
            this.elements[0] = element;
            this.count++;
        } else {
            increase();
            addToBegin(element);
        }
    }

    public void remove(int element) {
        for(int i = 0; i < this.count; i++){
            if(element == this.elements[i]){
                for(int j = i; j < count; j++){
                    this.elements[j] = this.elements[j + 1];
                }
                this.elements[count] = 0;
                count--;
            }
        }
    }

    public void removeByIndex(int index) {
        for(int j = index; j < count; j++){
            this.elements[j] = this.elements[j + 1];
        }
        this.elements[count] = 0;
        count--;
    }

    public boolean contains(int element) {
        for(int i = 0; i < count; i++){
            if(element == elements[i]){
                return true;
            }
        }
        return false;
    }

    public int size() {
        return count;
    }

    public void increase(){
        int temp[] = new int[DEFAULT_ARRAY_SIZE + 5];
        for(int i = 0; i < this.elements.length; i++){
            temp[i] = this.elements[i];
        }
        this.elements = temp;
    }

}
