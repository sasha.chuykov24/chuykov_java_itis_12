package linked;

public class LinkedList {
    Node first;
    Node last;
    private int count;

    // nested (static nested)
    private static class Node {
        private int value;
        private Node next;

        public Node(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }

    }

    // inner (non-static nested)
    class LinkedListIterator {
        Node current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public int next() {
            int value = current.value;
            current = current.next;
            return value;
        }
    }



    public LinkedList() {

    }


    public void add(int element) {
        Node newNode = new Node(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
//            Node current = first;
//            while (current.getNext() != null) {
//                current = current.getNext();
//            }
//            current.setNext(newNode);
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    public int get(int index) {
        if (index >= 0 && index < count) {
            Node current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return -1;
        }
    }

    public void addToBegin(int element) {
        Node newNode = new Node(element);
        newNode.setNext(first);
        first = newNode;
        count++;
    }

    public void remove(int element) {
        Node current = first;
        for(int i = 1; i < count; i++){
            if(current.getNext().getValue() == element){
                if(current.getNext().getNext() == null){
                    current.setNext(null);
                    this.last = current;
                    count--;
                } else {
                    current.setNext(current.getNext().getNext());
                    count--;
                }
            } else if(first.getValue() == element){
                first = current.getNext();
                count--;
            } else {
                current = current.getNext();
            }
        }
    }

    public void removeByIndex(int index) {
        Node current = first;
        for(int i = 1; i < count; i++){
            if(index == i + 1){
                if(current.getNext().getNext() == null){
                    current.setNext(null);
                    this.last = current;
                    count--;
                } else {
                    current.setNext(current.getNext().getNext());
                    count--;
                }
            } else if(index == 1){
                first = current.getNext();
                count--;
            } else {
                current = current.getNext();
            }
        }
    }

    public boolean contains(int element) {
        Node current = first;
        for(int i = 1; i <= count; i++){
            if(element == current.getValue()){
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    public int size() {
        return count;
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }

}
