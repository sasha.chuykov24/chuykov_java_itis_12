package linked;

public class MainForLinkedList {
    public static void main(String[] args) {

        LinkedList list = new LinkedList();

        list.add(10);
        list.add(15);
        list.add(25);
        list.add(14);
        list.add(8);
        list.addToBegin(9);
        System.out.println(list.contains(10));
        list.removeByIndex(3);
        list.remove(20);

        LinkedList.LinkedListIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }



    }
}
