import java.util.Arrays;

public class Homework11 {
    public static void main(String[] args) {
        char mas[] = {'H', 'z', 'h', 'z', 'h', 'Z', 'a', 'a', 'L', 'A', 'a', 'L', 'h'};
        int mas1[] = new int[26];
        int max = 0;


        for (int i = 0; i < mas.length; i++) {
            if (mas[i] >= 'A' && mas[i] <= 'Z'){
                mas1[(int) mas[i] - 65]++;
            }
            if(mas[i] >= 'a' && mas[i] <= 'z') {
                mas1[(int) mas[i] - 65 - 32]++;
            }
        }

        System.out.println(Arrays.toString(mas));
        System.out.println(Arrays.toString(mas1));

        //поиск наибольшего числа (количества)
        max = mas1[0];
        for (int i = 1; i < mas1.length; i++){
            if(mas1[i] > max){
                max = mas1[i];
            }
        }
        //вывод
        for(int i = 0; i < mas1.length; i++){
            if(mas1[i] == max){
                System.out.println((char) (i + 65) + " - " + mas1[i]);
            }
        }
    }
}
