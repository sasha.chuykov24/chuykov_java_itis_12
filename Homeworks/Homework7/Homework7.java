class Homework7{
	public static int sumOfNumerals(int number){
		int sum = 0;
		while(number != 0){
			sum = sum + number % 10;
			number = number / 10;
		}
		return sum;
	}
	public static int reversed(int num){
		int reversed = 0;
		while(num != 0){
			reversed = reversed * 10 + num % 10;
			num = num / 10;
		}
		return reversed;
	}
	public static int sumFromAToB(int a, int b){
		int sum = 0;
		for (int i=a; i<=b; i++){
			sum = sum + i;
		}
		return sum;
	}
	public static void main(String[] args){
		System.out.println(sumOfNumerals(34));
		System.out.println(reversed(1234));
		System.out.println(sumFromAToB(1, 5));
	}
}