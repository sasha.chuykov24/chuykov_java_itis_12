public class Channel {
    private int number;
    private Telecast telecasts[];

    public Channel(int number, Telecast[] telecasts) {
        this.number = number;
        this.telecasts = telecasts;

    }



    public int getNumber() {
        return number;
    }

    public Telecast[] getTelecasts() {
        return telecasts;
    }

    public Telecast getTelecast(int i) {
        return telecasts[i];
    }

}
