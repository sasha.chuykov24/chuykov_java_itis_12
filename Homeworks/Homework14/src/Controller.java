import java.util.Scanner;

public class Controller {
    private Television television;

    public Controller(Television television) {
        this.television = television;
    }

    public Television getTelevision() {
        return television;
    }

    public void chooseChannel() {
        int n;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter button: ");
        printMenu();
        int numberOfButton;
        boolean exit = false;
        while (!exit) {
            numberOfButton = scanner.nextInt();
            switch (numberOfButton) {
                case 0:
                    System.out.println(playingChannel(numberOfButton));
                    break;
                case 1:
                    System.out.println(playingChannel(numberOfButton));
                    break;
                case 2:
                    printMenu();
                    break;
                case 3:
                    System.out.println("Switching off");
                    exit = true;
                    break;
            }
        }
    }

    private String playingChannel(int numberOfButton){
        String str;
        if(television.playingTelecast(numberOfButton).getName() == null){
            str = "Технический перерыв";
        } else {
            str = television.playingTelecast(numberOfButton).getName() + " is now playing";
        }
        return str;
    }

    private void printMenu(){
        System.out.println("Channels: 0 and 1 \n" +
                "2 - print menu \n" +
                "3 - switch off TV");
    }
}
