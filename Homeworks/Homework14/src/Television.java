import java.time.LocalTime;

public class Television {
    private Channel channels[];
    private LocalTime currentTime;

    public Television(Channel[] channels) {
        this.channels = channels;
        this.currentTime = LocalTime.of(9,45);
    }

    public Channel[] getChannels() {
        return channels;
    }

    public Telecast playingTelecast(int numberOfChannel) {
        for (int i = 0; i < channels[numberOfChannel].getTelecasts().length; i++) {
            if (channels[numberOfChannel].getTelecast(i).getStartTime().isBefore(currentTime) &&
                    channels[numberOfChannel].getTelecast(i).getEndTime().isAfter(currentTime)) {
                return channels[numberOfChannel].getTelecast(i);
            }

        }
        return null;
    }
}
