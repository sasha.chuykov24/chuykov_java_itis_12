import java.time.LocalTime;

public class Homework14 {
    public static void main(String[] args) {
        Telecast telecast1 = new Telecast("News", LocalTime.of(8,0),
                LocalTime.of(8,59));
        Telecast telecast2 = new Telecast("Serial", LocalTime.of(9,0),
                LocalTime.of(9,59));
        Telecast telecast3 = new Telecast("Film", LocalTime.of(10,0),
                LocalTime.of(11,30));

        Telecast telecast11 = new Telecast("Food Show", LocalTime.of(8,0),
                LocalTime.of(8,59));
        Telecast telecast22 = new Telecast("Cartoons", LocalTime.of(9,0),
                LocalTime.of(9,59));
        Telecast telecast33 = new Telecast("Hell Kitchen", LocalTime.of(10,0),
                LocalTime.of(11,30));

        Telecast[] masTelecast1 = {telecast1, telecast2, telecast3};
        Channel channel1 = new Channel(1, masTelecast1);

        Telecast[] masTelecast2 = {telecast11, telecast22, telecast33};
        Channel channel2 = new Channel(2, masTelecast2);

        Channel[] channels = {channel1, channel2};
        Television television = new Television(channels);

        Controller controller = new Controller(television);
        controller.chooseChannel();
    }
}
