package ru.itis.java.app;

import com.beust.jcommander.JCommander;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Homework20 {
    public static void main(String[] args) {
        List<FileInformation> list1 = new ArrayList<>();
        list1.add(new FileInformation("Documents", "500"));
        list1.add(new FileInformation("Videos", "1000"));
        list1.add(new FileInformation("Images", "400"));
        list1.add(new FileInformation("Animals", "200"));

        List<FileInformation> list2 = new ArrayList<>();
        list2.add(new FileInformation("Notes", "150"));
        list2.add(new FileInformation("Streams", "2000"));
        list2.add(new FileInformation("Screenshots", "500"));
        list2.add(new FileInformation("Flowers", "400"));

        List<FileInformation> list3 = new ArrayList<>();
        list3.add(new FileInformation("Mouse", "20"));
        list3.add(new FileInformation("Keyboard", "35"));
        list3.add(new FileInformation("Screen", "15"));
        list3.add(new FileInformation("Headphone", "34"));

        Map<String, List<FileInformation>> mapOfFolders = new HashMap<>();
        mapOfFolders.put("Desktop1", list1);
        mapOfFolders.put("Desktop2", list2);
        mapOfFolders.put("Desktop3", list3);

        FolderMap folderMap = new FolderMap(mapOfFolders);

        ExecutorService executorService = Executors.newFixedThreadPool(2);
        Arguments arguments = new Arguments();

        JCommander.newBuilder()
                .addObject(arguments)
                .build()
                .parse(args);

        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName() + " " + folderMap.getFolder(arguments.folder1));
        });

        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName() + " " + folderMap.getFolder(arguments.folder2));
        });

        executorService.submit(() -> {
            System.out.println(Thread.currentThread().getName() + " " + folderMap.getFolder(arguments.folder3));
        });

        executorService.shutdown();
    }
}
