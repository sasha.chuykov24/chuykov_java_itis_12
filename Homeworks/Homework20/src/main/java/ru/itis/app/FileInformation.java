package ru.itis.java.app;

import java.util.Objects;

public class FileInformation {
    String fileName;
    String size;

    public FileInformation(String fileName, String size) {
        this.fileName = fileName;
        this.size = size;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileInformation that = (FileInformation) o;
        return Objects.equals(fileName, that.fileName) &&
                Objects.equals(size, that.size);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, size);
    }

    @Override
    public String toString() {
        return "{" +
                "fileName='" + fileName + '\'' +
                ", size='" + size + '\'' +
                '}';
    }
}
