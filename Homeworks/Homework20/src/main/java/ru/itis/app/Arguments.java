package ru.itis.java.app;

import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Arguments {

        @Parameter(names = {"--folder1"})
        public String folder1;

        @Parameter(names = {"--folder2"})
        public String folder2;

        @Parameter(names = {"--folder3"})
        public String folder3;
    }
