package ru.itis.java.app;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FolderMap {
    Map<String, List<FileInformation>> mapOfFolders;

    public FolderMap(Map<String, List<FileInformation>> mapOfFolders) {
        this.mapOfFolders = mapOfFolders;
    }

    public Map<String, List<FileInformation>> getFolder(String name) {
        Map<String, List<FileInformation>> tempMap = new HashMap<>();
        tempMap.put(name, mapOfFolders.get(name));
        return tempMap;
    }




}
