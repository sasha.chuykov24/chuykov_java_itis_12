import java.util.Scanner;

class Homework4 {
	public static void main(String[] args){
		Scanner scanner =  new Scanner(System.in);
		int mult = 1;
		int sum = 0;
		int number = scanner.nextInt();
		while(number != -1){
			int number1 = number;
			while(number1 != 0) {
			    int a = number1 % 10;
			    mult = mult * a;
			    number1 = number1 / 10;
		    }
		    if(mult % 3 == 0 && mult != 0){
		    	sum = sum + number;
		    }
				number = scanner.nextInt();
			}
		System.out.println("Sum:" + sum);
		}
}