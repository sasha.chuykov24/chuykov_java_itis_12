public interface IList<I> {
    void add(I element);
    I get(int index);
    void addToBegin(I element);
    void remove(I element);
    void removeByIndex(int index);
    boolean contains(I element);
    int size();
}
