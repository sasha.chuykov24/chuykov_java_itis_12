public class Main {
    public static void main(String[] args) {

        LinkedList<Integer> list = new LinkedList<>();

        list.add(10);
        list.add(15);
        list.add(25);
        list.add(14);
        list.add(8);
        list.addToBegin(9);
        System.out.println(list.contains(10));
        list.removeByIndex(3);
        list.remove(20);

        LinkedList.LinkedListIterator iterator = list.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }


        LinkedList<String> listString = new LinkedList<>();

        listString.add("Sasha");
        listString.add("Masha");
        listString.add("Kol9I");
        listString.add("Victor");
        listString.add("Elizabeth");
        listString.addToBegin("List");
        System.out.println(listString.contains("Sasha"));
        listString.removeByIndex(4);

        LinkedList.LinkedListIterator iteratorString = listString.iterator();
        while (iteratorString.hasNext()) {
            System.out.println(iteratorString.next());
        }


    }
}
