public class LinkedList<T> implements IList<T>{
    Node<T> first;
    Node<T> last;
    private int count;

    // nested (static nested)
    private static class Node<N> {
        private N value;
        private Node<N> next;

        public Node(N value) {
            this.value = value;
        }

        public N getValue() {
            return this.value;
        }

        public void setValue(N value) {
            this.value = value;
        }

        public Node<N> getNext() {
            return this.next;
        }

        public void setNext(Node<N> next) {
            this.next = next;
        }

    }

    // inner (non-static nested)
    class LinkedListIterator {
        Node<T> current;

        public LinkedListIterator() {
            this.current = first;
        }

        public boolean hasNext() {
            return current != null;
        }

        public T next() {
            T value = current.value;
            current = current.next;
            return value;
        }
    }

    public LinkedList() {

    }

    @Override
    public void add(T element) {
        Node<T> newNode = new Node<>(element);
        if (first == null) {
            first = newNode;
            last = newNode;
        } else {
            last.setNext(newNode);
            last = newNode;
        }
        count++;
    }

    @Override
    public T get(int index) {
        if (index >= 0 && index < count) {
            Node<T> current = first;
            for (int i = 1; i <= index; i++) {
                current = current.getNext();
            }
            return (T) current.getValue();
        } else {
            System.out.println("Нет такого элемента");
            return null;
        }
    }

    @Override
    public void addToBegin(T element) {
        Node<T> newNode = new Node<>(element);
        newNode.setNext(first);
        first = newNode;
        count++;
    }

    @Override
    public void remove(T element) {
        Node<T> current = first;
        for(int i = 1; i < count; i++){
            if(current.getNext().getValue().equals(element)){
                if(current.getNext().getNext() == null){
                    current.setNext(null);
                    this.last = current;
                    count--;
                } else {
                    current.setNext(current.getNext().getNext());
                    count--;
                }
            } else if(first.getValue() == element){
                first = current.getNext();
                count--;
            } else {
                current = current.getNext();
            }
        }
    }

    @Override
    public void removeByIndex(int index) {
        Node<T> current = first;
        for(int i = 1; i < count; i++){
            if(index == i + 1){
                if(current.getNext().getNext() == null){
                    current.setNext(null);
                    this.last = current;
                    count--;
                } else {
                    current.setNext(current.getNext().getNext());
                    count--;
                }
            } else if(index == 1){
                first = current.getNext();
                count--;
            } else {
                current = current.getNext();
            }
        }
    }

    @Override
    public boolean contains(T element) {
        Node<T> current = first;
        for(int i = 1; i <= count; i++){
            if(element.equals(current.getValue())){
                return true;
            }
            current = current.getNext();
        }
        return false;
    }

    @Override
    public int size() {
        return count;
    }

    public LinkedListIterator iterator() {
        return new LinkedListIterator();
    }
}
