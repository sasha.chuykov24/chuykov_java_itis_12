package ru.itis12;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.itis12.config.ApplicationConfig;

public class Main {

    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        ChatExample chatExample = context.getBean(ChatExample.class);
        chatExample.start(7777);
    }
}

