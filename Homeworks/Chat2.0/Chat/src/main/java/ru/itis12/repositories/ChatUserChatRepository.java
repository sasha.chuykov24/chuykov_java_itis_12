package ru.itis12.repositories;

public interface ChatUserChatRepository {
    void save(Integer user_id, Integer chat_id);
}
