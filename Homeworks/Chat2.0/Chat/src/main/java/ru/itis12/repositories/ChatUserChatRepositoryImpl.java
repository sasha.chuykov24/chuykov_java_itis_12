package ru.itis12.repositories;

import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@Component
public class ChatUserChatRepositoryImpl implements ChatUserChatRepository {

    private DataSource dataSource;

    //language=SQL
    private static final String SQL_INSERT = "insert into chat_user_chat(user_id, chat_id) values (?,?)";

    public ChatUserChatRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    public void save(Integer user_id, Integer chat_id) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT);
            preparedStatement.setInt(1,user_id);
            preparedStatement.setInt(2,chat_id);
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }
}
