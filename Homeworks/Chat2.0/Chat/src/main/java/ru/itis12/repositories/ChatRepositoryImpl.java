package ru.itis12.repositories;

import org.springframework.stereotype.Component;
import ru.itis12.models.Chat;

import javax.sql.DataSource;
import java.sql.*;
import java.util.List;

@Component
public class ChatRepositoryImpl implements ChatRepository {

    private DataSource dataSource;

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select * from chat where name = ";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from chat where id = ";

    //language=SQL
    private static final String SQL_INSERT_CHAT = "insert into chat(name) values (?)";

    public ChatRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    /*private RowMapper<Chat> chatRowMapper = new RowMapper<Chat>() {
        public Chat mapRow(ResultSet row) throws SQLException{
            return new Chat(
                    row.getInt("id"),
                    row.getString("name"),
                    row.getString("startDate")
            );
        }
    };*/

    @Override
    public void save(Chat object) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT_CHAT);
            statement.setString(1, object.getName());
            statement.executeUpdate();
            statement.close();
        } catch (SQLException e){
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Chat object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Chat find(Integer id) {
        try{
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_ID + "'" + id + "'");
            if(row.next()){
                return new Chat(row.getInt("id"),row.getString("name"));
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public Chat findByName(String name) {
        try{
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet row = statement.executeQuery(SQL_SELECT_BY_NAME + "'" + name + "'");
            if(row.next()){
                return new Chat(row.getInt("id"),row.getString("name"));
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<Chat> findAll() {
        return null;
    }
}
