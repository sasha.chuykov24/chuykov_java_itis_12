package ru.itis12.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class User {
    private Integer id;
    private String nickname;
    private List<Integer> idsOfChats = new ArrayList<>();

    public User(String nickname){
        this.nickname = nickname;
    }

    public User(Integer id, String nickname, Integer chat_id) {
        this.id = id;
        this.nickname = nickname;
        this.idsOfChats.add(0,chat_id);
    }

    public User(Integer id, String nickname) {
        this.id = id;
        this.nickname = nickname;
    }

    public List<Integer> getChats() {
        return idsOfChats;
    }

    public Integer getLastChatId(){
        return !idsOfChats.isEmpty() ? idsOfChats.get(0) : null;
    }
}
