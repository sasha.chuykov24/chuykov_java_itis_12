package ru.itis12.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder

public class Message {
    private String text;
    private Integer userId;
    private Integer chatId;
    private Timestamp time;

    public Message(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "\n" + text;
    }
}
