package ru.itis12.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis12.models.Chat;
import ru.itis12.models.Message;
import ru.itis12.models.User;
import ru.itis12.repositories.*;

import java.util.List;

@Component
public class ChatServiceImpl implements ChatService{
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private ChatRepository chatRepository;
    @Autowired
    private MessageRepository messageRepository;
    @Autowired
    private ChatUserChatRepository chatUserChatRepository;


    @Override
    public User findUserByNickname(String nickname) {
        return usersRepository.findByNickname(nickname);
    }

    @Override
    public void addUser(User user) {
        usersRepository.save(user);
    }

    @Override
    public Chat findChatById(Integer id) {
        return chatRepository.find(id);
    }

    @Override
    public Chat findChatByName(String name) {
        return chatRepository.findByName(name);
    }

    @Override
    public void addChat(Chat chat) {
        chatRepository.save(chat);
    }

    @Override
    public List<Message> findLast30Messages(Integer roomId) {
        return messageRepository.findLast30(roomId);
    }

    @Override
    public void saveMessage(Message message) {
        messageRepository.save(message);
    }

    @Override
    public void bindUserIdAndChatId(Integer userId, Integer chatId) {
        chatUserChatRepository.save(userId, chatId);
    }
}
