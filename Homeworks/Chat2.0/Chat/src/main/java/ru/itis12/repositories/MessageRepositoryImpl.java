package ru.itis12.repositories;

import org.springframework.stereotype.Component;
import ru.itis12.models.Message;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

@Component
public class MessageRepositoryImpl implements MessageRepository {

    //language=SQl
    private static final String SQL_INSERT_MESSAGE = "insert into message (text, user_id, chat_id, time) values (?,?,?,?)";

    //language=SQl
    private static final String SQL_SELECT_30_MESSAGES_BY_CHAT_ID = "select m.text as text,\n" +
            "       m.user_id as user_id,\n" +
            "       m.chat_id as chat_id,\n" +
            "       m.time as time,\n" +
            "       cu.nickname as nickname\n" +
            "from message m\n" +
            "left join chat_user cu on m.user_id = cu.id\n" +
            "where chat_id = ?\n" +
            "order by time desc\n" +
            "limit 30;";

    private DataSource dataSource;

    public MessageRepositoryImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @Override
    public void save(Message object) {
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_INSERT_MESSAGE);
            preparedStatement.setString(1, object.getText());
            preparedStatement.setInt(2, object.getUserId());
            preparedStatement.setInt(3, object.getChatId());
            preparedStatement.setTimestamp(4,object.getTime());
            preparedStatement.executeUpdate();
            preparedStatement.close();
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void update(Message object) {

    }

    @Override
    public void delete(Integer id) {

    }

    @Override
    public Message find(Integer id) {
        return null;
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public List<Message> findLast30(Integer id) {
        List<Message> messages = new LinkedList<>();
        try {
            Connection connection = dataSource.getConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SQL_SELECT_30_MESSAGES_BY_CHAT_ID);
            preparedStatement.setInt(1,id);
            ResultSet row = preparedStatement.executeQuery();
            while(row.next()){
                messages.add(0, new Message(row.getString("nickname") + ": " +
                        row.getString("text")));
            }
            return messages;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
