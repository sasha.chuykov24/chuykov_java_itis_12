package ru.itis12.services;

import ru.itis12.models.Chat;
import ru.itis12.models.Message;
import ru.itis12.models.User;

import java.util.List;

public interface ChatService {
    User findUserByNickname(String nickname);
    void addUser(User user);

    Chat findChatById(Integer id);
    Chat findChatByName(String name);
    void addChat(Chat chat);

    List<Message> findLast30Messages(Integer id);
    void saveMessage(Message message);

    void bindUserIdAndChatId(Integer userId, Integer chatId);
}
