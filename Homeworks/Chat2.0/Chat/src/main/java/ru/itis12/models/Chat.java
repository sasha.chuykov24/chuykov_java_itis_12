package ru.itis12.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data

public class Chat {
    private Integer id;
    private String name;
    private List<User> users;

    public Chat(Integer id, String name) {
        this.id = id;
        this.name = name;
        this.users = new ArrayList<>();
    }

    public Chat (String name){
        this.name = name;
    }
}
