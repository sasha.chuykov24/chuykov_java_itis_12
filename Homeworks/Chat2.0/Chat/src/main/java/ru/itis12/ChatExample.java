package ru.itis12;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.itis12.models.Chat;
import ru.itis12.models.Message;
import ru.itis12.models.User;
import ru.itis12.services.ChatService;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ChatExample {

    @Autowired
    private ChatService chatService;

    private List<SocketClient> clientsWithoutRooms;
    private Map<String, List<SocketClient>> clientsInRooms;


    public ChatExample() {
        this.clientsWithoutRooms = new ArrayList<>();
        this.clientsInRooms = new HashMap<>();
    }

    public void start(int port) {
        try {
            ServerSocket serverSocket = new ServerSocket(port);
            while (true) {
                Socket client = serverSocket.accept();
                SocketClient socketClient = new SocketClient(client);
                this.clientsWithoutRooms.add(socketClient);
                socketClient.start();
            }
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

    }

    public class SocketClient extends Thread {
        private Socket client;
        private BufferedReader from;
        private PrintWriter to;
        private String currentRoom;
        private Integer roomId;
        private User currentUser;
        private Integer userId;


        public SocketClient(Socket socket) {
            this.client = socket;
            try {
                InputStream clientInputStream = client.getInputStream();
                this.from = new BufferedReader(new InputStreamReader(clientInputStream));
                this.to = new PrintWriter(client.getOutputStream(), true);

            } catch (IOException e) {
                throw new IllegalArgumentException(e);
            }
        }

        @Override
        public void run() {
            int phase = 0;
            while (true) {
                switch (phase) {
                    case 0:
                        try {
                            to.println("Введите nickname: ");
                            String nickname = from.readLine();

                            while (nickname == null) {
                                nickname = this.from.readLine();
                            }

                            this.currentUser = chatService.findUserByNickname(nickname);

                            if (this.currentUser == null) {
                                this.currentUser = new User(nickname);
                                chatService.addUser(this.currentUser);
                                phase = 1;
                            } else {
                                roomId = this.currentUser.getLastChatId();
                                userId = this.currentUser.getId();
                                this.currentRoom = chatService.findChatById(roomId).getName();
                                if (!clientsInRooms.containsKey(currentRoom)) {
                                    clientsInRooms.put(currentRoom, new ArrayList<>());
                                }
                                clientsInRooms.get(currentRoom).add(this);
                                phase = 2;
                            }
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                        break;
                    case 1:
                        try {
                            to.println("Введите комнату: ");

                            String roomName = from.readLine();

                            while (roomName == null) {
                                roomName = from.readLine();
                            }

                            if (chatService.findChatByName(roomName) == null) {
                                chatService.addChat(new Chat(roomName));
                            }

                            if (!clientsInRooms.containsKey(roomName)) {
                                clientsInRooms.put(roomName, new ArrayList<>());
                            }
                            this.currentRoom = roomName;
                            clientsInRooms.get(currentRoom).add(this);
                            roomId = chatService.findChatByName(currentRoom).getId();
                            userId = chatService.findUserByNickname(this.currentUser.getNickname()).getId();
                            chatService.bindUserIdAndChatId(userId, roomId);
                            phase = 2;
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                        break;
                    case 2:
                        try {
                            to.println(chatService.findLast30Messages(roomId));
                            String message = from.readLine();
                            while (message != null) {
                                if (message.startsWith("exit from room")) {
                                    phase = 1;
                                    clientsInRooms.get(currentRoom).remove(this);
                                    message = null;
                                } else {
                                   chatService.saveMessage(new Message(message, userId, roomId, new Timestamp(System.currentTimeMillis())));
                                    for (SocketClient anotherClient : clientsInRooms.get(currentRoom)) {
                                        anotherClient.to.println(this.currentUser.getNickname() + ": " + message);
                                        message = from.readLine();
                                    }
                                }
                            }
                        } catch (IOException e) {
                            throw new IllegalArgumentException(e);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
