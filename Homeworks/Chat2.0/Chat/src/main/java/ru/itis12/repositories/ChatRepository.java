package ru.itis12.repositories;

import ru.itis12.models.Chat;

public interface ChatRepository extends CrudRepository<Chat> {
    Chat findByName(String Name);
}
