package ru.itis12.repositories;

import org.springframework.stereotype.Component;
import ru.itis12.models.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
public class UsersRepositoryJdbcImpl implements UsersRepository {

    //language=SQL
    private static final String SQL_SELECT_ALL = "select * from chat_user";

    //language=SQL
    private static final String SQL_SELECT_BY_ID = "select * from chat_user where id = ";

    //language=SQL
    private static final String SQL_SELECT_BY_NAME = "select cu.id as user_id,\n" +
            "       cu.nickname as user_nickname,\n" +
            "       cuc.chat_id as chat_id\n" +
            "from chat_user cu\n" +
            "    left join chat_user_chat cuc on cu.id = cuc.user_id\n" +
            "where nickname = ";

    //language=SQL
    private static final String SQL_INSERT_USER = "insert into chat_user(nickname) values (?)";

    private DataSource dataSource;

    private RowMapper<User> userRowMapper = new RowMapper<User>() {
        public User mapRow(ResultSet row) throws SQLException{
            return new User(
                    row.getInt("user_id"),
                    row.getString("user_nickname"),
                    row.getInt("chat_id")
            );
        }
    };

    public UsersRepositoryJdbcImpl(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public User findByFirstName(String firstName) {
        return null;
    }

    public void save(User object) {
        PreparedStatement statement = null;
        try {
            Connection connection = dataSource.getConnection();
            statement = connection.prepareStatement(SQL_INSERT_USER);
            statement.setString(1, object.getNickname());
            statement.executeUpdate();
        } catch (SQLException e){
            throw new IllegalArgumentException();
        } finally {
            if(statement != null){
                try{
                    statement.close();
                } catch (SQLException ignored){
                }
            }
        }
    }

    public void update(User object) {

    }

    public void delete(Integer id) {

    }

    public User find(Integer id) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_ID + id);
            resultSet.next();
            statement.close();
            return userRowMapper.mapRow(resultSet);
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public User findByNickname(String nickName) {
        try {
            Connection connection = dataSource.getConnection();
            Statement statement = connection.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_BY_NAME + "'" + nickName + "'");
            if(resultSet.last()){
                return userRowMapper.mapRow(resultSet);
            }
            return null;
        } catch (SQLException e) {
            throw new IllegalArgumentException(e);
        }
    }

    public List<User> findAll() {
        try {
            Connection connection = dataSource.getConnection();
            List<User> result = new ArrayList<>();
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SQL_SELECT_ALL);
            while(resultSet.next()){
                User user = userRowMapper.mapRow(resultSet);
                result.add(user);
            }
            return result;
        } catch (SQLException e) {
            throw new IllegalArgumentException();
        }
    }
}
